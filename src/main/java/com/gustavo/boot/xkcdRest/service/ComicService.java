/**
 * 
 */
package com.gustavo.boot.xkcdRest.service;

import java.net.URL;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gustavo.boot.xkcdRest.domain.Comic;

/**
 * Revision History:
 * 		@author Gustavo Rodrigues, 2019.09.18
 *
 */
@Service
public class ComicService {
	
	public Comic getLatestComic() {
		String url = "http://dynamic.xkcd.com/api-0/jsonp/comic/";
		try {
			return new ObjectMapper().setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY).readValue(new URL(url).openStream(), Comic.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
