/**
 * 
 */
package com.gustavo.boot.xkcdRest.domain;

/**
 * Revision History:
 * 		@author Gustavo Rodrigues, 2019.09.18
 *
 */
public class Comic {
	private String news;
	private String link;
	private String img;
	private String num;
	private String day;
	private String year;
	private String month;
	private String title;
	private String safe_title;
	private String transcript;
	private String alt;
	
	protected Comic() {}

	public Comic(String news, String link, String img, String num, String day, String year, String month, String title,
			String safe_title, String transcript, String alt) {
		this.news = news;
		this.link = link;
		this.img = img;
		this.num = num;
		this.day = day;
		this.year = year;
		this.month = month;
		this.title = title;
		this.safe_title = safe_title;
		this.transcript = transcript;
		this.alt = alt;
	}

	public String getNews() {
		return news;
	}

	public String getLink() {
		return link;
	}

	public String getImg() {
		return img;
	}

	public String getNum() {
		return num;
	}

	public String getDay() {
		return day;
	}

	public String getYear() {
		return year;
	}

	public String getMonth() {
		return month;
	}

	public String getTitle() {
		return title;
	}

	public String getSafe_title() {
		return safe_title;
	}

	public String getTranscript() {
		return transcript;
	}

	public String getAlt() {
		return alt;
	}

	@Override
	public String toString() {
		return "Comic [url = https://www.xkcd.com/"+num+"\n"
				+ "title: " + title+"\n"
				+ "img: " + img+"\n"
				+ "alt: "+ alt + "]";
	}
	
}
