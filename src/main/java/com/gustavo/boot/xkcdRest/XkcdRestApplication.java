package com.gustavo.boot.xkcdRest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XkcdRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(XkcdRestApplication.class, args);
	}

}
