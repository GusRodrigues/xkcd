/**
 * 
 */
package com.gustavo.boot.xkcdRest.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.gustavo.boot.xkcdRest.service.ComicService;

/**
 * Revision History:
 * 		@author Gustavo Rodrigues, 2019.09.18
 *
 */
@Controller
@RequestMapping("/xkcd")
public class ComicController {
	private ComicService service;
	
	
	public ComicController(ComicService service) {
		this.service = service;
	}


	@RequestMapping("/")
	public ModelAndView test() {
		return new ModelAndView("index.html").addObject("comic", service.getLatestComic());
	}
}
